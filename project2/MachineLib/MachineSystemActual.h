/**
 * @file MachineSystemActual.h
 * @author rebec
 *
 *
 */
#include "MachineSystem.h"
#ifndef CANADIANEXPERIENCE_MACHINELIB_MACHINESYSTEMACTUAL_H
#define CANADIANEXPERIENCE_MACHINELIB_MACHINESYSTEMACTUAL_H

class MachineSystemActual : public MachineSystem
{
private:
	double mFrameRate = 0;
	double mTime = 0;
	wxPoint mLocation = wxPoint(0,0);
	int mFrame;
public:
	void DrawMachine(std::shared_ptr<wxGraphicsContext> graphics);
	void SetTime(double time){ mTime = time; }
};

#endif //CANADIANEXPERIENCE_MACHINELIB_MACHINESYSTEMACTUAL_H
