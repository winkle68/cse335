/**
 * @file Machine.cpp
 * @author rebec
 */
#include "pch.h"

#include <wx/valnum.h>
#include <wx/stdpaths.h>

#include "Machine.h"
Machine::Machine()
{
}
void Machine::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{

	double wid = mBitmap->GetWidth();
	double hit = mBitmap->GetHeight();
	graphics->DrawBitmap(*mBitmap, 0,0,wid,hit);
}
