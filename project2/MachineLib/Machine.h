/**
 * @file Machine.h
 * @author rebec
 *
 *
 */
#include "MachineSystemActual.h"
#ifndef CANADIANEXPERIENCE_MACHINELIB_MACHINE_H
#define CANADIANEXPERIENCE_MACHINELIB_MACHINE_H

class Machine
{
private:
	MachineSystemActual* mSystem;
	std::unique_ptr<wxImage> mImage;
	std::unique_ptr<wxBitmap> mBitmap;
public:
	Machine();
	void Draw(std::shared_ptr<wxGraphicsContext> graphics);
	void SetTime(double time){ mSystem->SetTime(time); }

};

#endif //CANADIANEXPERIENCE_MACHINELIB_MACHINE_H
