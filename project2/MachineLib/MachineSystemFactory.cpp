/**
 * @file MachineSystemFactory.cpp
 *
 * @author Charles Owen
 *
 * You are allowed to change this file.
 */
#include <ImageDrawable.h>
#include "pch.h"
#include "MachineSystemFactory.h"
#include "Machine.h"
#include "MachineSystemActual.h"
using namespace std;

/// Directory within resources that contains the images.
const std::wstring ImagesDirectory = L"/images";
/**
 * Constructor
 * @param resourcesDir Directory to load resources from
 * @param audioEngine The miniaudio engine to use
 */
MachineSystemFactory::MachineSystemFactory(std::wstring resourcesDir, ma_engine* audioEngine) :
    mResourcesDir(resourcesDir), mAudioEngine(audioEngine)
{
}


/**
 * Create a machine system object
 *
 * Do not change the return type of this function!
 *
 * @return Object of type MachineSystem
 */
std::shared_ptr<MachineSystem> MachineSystemFactory::CreateMachineSystem()
{
	auto imagesDir = mResourcesDir + ImagesDirectory;
	auto system = std::make_shared<MachineSystemActual>();
	auto background = make_shared<Machine>();
	//auto backgroundI =
		make_shared<ImageDrawable>(L"Background", imagesDir + L"/mortier.png");

	return system;
	/*
	 *auto imagesDir = resourcesDir + ImagesDirectory;

    shared_ptr<Picture> picture = make_shared<Picture>();

    // Create the background and add it
    auto background = make_shared<Actor>(L"Background");
    background->SetClickable(false);
    background->SetPosition(wxPoint(0, 0));
    auto backgroundI =
            make_shared<ImageDrawable>(L"Background", imagesDir + L"/Background.jpg");
    background->AddDrawable(backgroundI);
    background->SetRoot(backgroundI);
    picture->AddActor(background);
	 */
}


